#ifndef __have__WordClockDisplay_h__
#define __have__WordClockDisplay_h__

#include <ClockFace.h>
#include <ledstrip/LedStripFrame.h>

#define NUM_LEDS 66

class WordClockFace: public ClockFace<LedStripFrame<NUM_LEDS>>  {
    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);

    private:
        const int nbLeds = NUM_LEDS;

        struct WORD_LED_INDEXES {
            int nbLeds;
            const int *leds;
        };

        struct TEXT_LED_INDEXES {
            int nbWords;
            const WORD_LED_INDEXES *w;
        };

        const WORD_LED_INDEXES MINUIT = { 3, new int[3] { 33, 34, 35 }};
        const WORD_LED_INDEXES H_1    = { 2, new int[2] {  1,  2 } };
        const WORD_LED_INDEXES H_2    = { 3, new int[3] {  3,  4,  5 } };
        const WORD_LED_INDEXES H_3    = { 3, new int[3] { 13, 12, 11 } };
        const WORD_LED_INDEXES H_4    = { 4, new int[4] { 30, 29, 28, 27 } };
        const WORD_LED_INDEXES H_5    = { 3, new int[3] { 17, 18, 19} };
        const WORD_LED_INDEXES H_6    = { 2, new int[2] { 10,  9 } };
        const WORD_LED_INDEXES H_7    = { 3, new int[3] { 22, 23, 24 } };
        const WORD_LED_INDEXES H_8    = { 2, new int[2] { 20, 21 } };
        const WORD_LED_INDEXES H_9    = { 3, new int[3] { 16, 15, 14} };
        const WORD_LED_INDEXES H_10   = { 2, new int[2] { 25, 26} };
        const WORD_LED_INDEXES H_11   = { 3, new int[3] {  6,  7, 8 } };
        const WORD_LED_INDEXES MIDI   = { 2, new int[2] { 32, 31 } };

        const WORD_LED_INDEXES M_5   = { 3, new int[3] { 54, 55, 56 } };
        const WORD_LED_INDEXES M_10  = { 2, new int[2] { 60, 61 } };
        const WORD_LED_INDEXES QUART = { 3, new int[3] { 57, 58, 59 } };
        const WORD_LED_INDEXES M_20  = { 3, new int[3] { 45, 44, 43 } };
        const WORD_LED_INDEXES DEMI  = { 4, new int[4] {40, 49, 50, 51} };
        const WORD_LED_INDEXES E     = { 1, new int[1] {52} };
        const WORD_LED_INDEXES MOINS = { 3, new int[3] { 48, 47, 46 }};
        const WORD_LED_INDEXES LE    = { 2, new int[2] { 42, 41 } };
        const WORD_LED_INDEXES ET    = { 1, new int[1] { 40 } };
        const WORD_LED_INDEXES S     = { 1, new int[1] { 39 } };
        const WORD_LED_INDEXES DASH  = { 1, new int[1] { 53 } };
        const WORD_LED_INDEXES HEURE = { 3, new int[3] { 36, 37, 38} };

        const WORD_LED_INDEXES DOT_1 = { 1, new int[1] { 62 } };
        const WORD_LED_INDEXES DOT_2 = { 1, new int[1] { 63 } };
        const WORD_LED_INDEXES DOT_3 = { 1, new int[1] { 64 } };
        const WORD_LED_INDEXES DOT_4 = { 1, new int[1] { 65 } };

        struct TEXT_LED_INDEXES HOUR_LEDS[13] = {
            {1, new WORD_LED_INDEXES[1] { MINUIT } },
            {2, new WORD_LED_INDEXES[2] { H_1,  HEURE} },
            {3, new WORD_LED_INDEXES[3] { H_2,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_3,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_4,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_5,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_6,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_7,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_8,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_9,  HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_10, HEURE, S} },
            {3, new WORD_LED_INDEXES[3] { H_11, HEURE, S} },
            {1, new WORD_LED_INDEXES[1] { MIDI } }
        };

        struct TEXT_LED_INDEXES MINUTE_LEDS[13] {
            { 0, new WORD_LED_INDEXES[0] {} },
            { 1, new WORD_LED_INDEXES[1] { M_5 } },
            { 1, new WORD_LED_INDEXES[1] { M_10 } },
            { 2, new WORD_LED_INDEXES[2] { ET,    QUART } },
            { 1, new WORD_LED_INDEXES[1] { M_20 } },
            { 3, new WORD_LED_INDEXES[3] { M_20,  DASH, M_5 } },
            { 3, new WORD_LED_INDEXES[3] { ET,    DEMI, E } },
            { 4, new WORD_LED_INDEXES[4] { MOINS, M_20, DASH, M_5 } },
            { 2, new WORD_LED_INDEXES[2] { MOINS, M_20 } },
            { 3, new WORD_LED_INDEXES[3] { MOINS, LE,   QUART } },
            { 2, new WORD_LED_INDEXES[2] { MOINS, M_10 } },
            { 2, new WORD_LED_INDEXES[2] { MOINS, M_5 }},
            { 2, new WORD_LED_INDEXES[2] { ET, DEMI } }
        };

        struct TEXT_LED_INDEXES MINUTE_DOTS_LEDS[5] {
            { 0, new WORD_LED_INDEXES[0] },
            { 1, new WORD_LED_INDEXES[1] { DOT_1} },
            { 2, new WORD_LED_INDEXES[2] { DOT_1, DOT_2} },
            { 3, new WORD_LED_INDEXES[3] { DOT_1, DOT_2, DOT_3} },
            { 4, new WORD_LED_INDEXES[4] { DOT_1, DOT_2, DOT_3, DOT_4} }
        };


        CRGB activeColorForLed(int ledNumber);
        CRGB inactiveColorForLed(int ledNumber);
        void activateLeds(LedStripFrame<NUM_LEDS>* frame, TEXT_LED_INDEXES word_leds);
        void deactivateAllLeds(LedStripFrame<NUM_LEDS>* frame);
};

#endif