#include "WordClockFace.h"

CRGB WordClockFace::activeColorForLed(int ledNumber) {
    return CHSV(255/nbLeds * ledNumber, 255, 255);
}

CRGB WordClockFace::inactiveColorForLed(int ledNumber) {
    return CHSV(255/nbLeds * ledNumber, 255, 2);
}

void WordClockFace::activateLeds(LedStripFrame<NUM_LEDS>* frame, TEXT_LED_INDEXES word_leds) {
    for(int i = 0; i < word_leds.nbWords; i++) {
        WORD_LED_INDEXES led_index = word_leds.w[i];
        for (int j = 0; j < led_index.nbLeds; j++)
            frame->setPixelAt(led_index.leds[j]-1, activeColorForLed(led_index.leds[j]-1));
    }
}

void WordClockFace::deactivateAllLeds(LedStripFrame<NUM_LEDS>* frame) {
    for(int i = 0; i < nbLeds; i++) {
        frame->setPixelAt(i, inactiveColorForLed(i));
    }    
}

LedStripFrame<NUM_LEDS> WordClockFace::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;

    int displayedMinute = minute - minute%5;
    int displayedHour = hour;

    if (displayedMinute > 30) {
        displayedHour = (hour + 1) % 24;
    }

    TEXT_LED_INDEXES hour_leds;
    if(displayedHour == 12) {
        hour_leds = HOUR_LEDS[12];  
    } else {
        hour_leds = HOUR_LEDS[displayedHour % 12];
    }

    TEXT_LED_INDEXES minute_leds;
    if(displayedMinute == 30 && (hour == 12 || hour == 0)) {
        minute_leds = MINUTE_LEDS[12];
    } else {
        minute_leds = MINUTE_LEDS[displayedMinute / 5];
    }

    TEXT_LED_INDEXES minute_dots = MINUTE_DOTS_LEDS[minute%5];

    deactivateAllLeds(&frame);
    activateLeds(&frame, hour_leds);
    activateLeds(&frame, minute_leds);
    activateLeds(&frame, minute_dots);

    frame.setDuration(100);

    return frame;
}