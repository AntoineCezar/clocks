# Gouge: a french word clock

Gouge is a clock that tell time in french. 
It's made from laser cut wood with acrylic inlays, an esp8266, 65 ws2812b leds and some other components.

![a word clock](images/2020.11.11-gouge.1024px.jpg)


## svg plans

Plans are split in different files:
* gouge-box.svg: 
  * 3mm plywood
  * the side and back plate of the box. 
* gouge-front_face_comfortaa.svg:
  * 3mm plywood
  * the front face with the french word.
* gouge-insert_acrylic-comfortaa.svg
  * 3mm transparent acrylic. 
  * individual letters
  * * these are made specifically to match my cutter kerf and cut angle. You might need to adjust those to have perfect fit.
* gouge-insert_wood-comfortaa.svg
  * 3mm plywood
  * the wood inserts that go inside the acrylic insert :)
  * these are made specifically to match my cutter kerf and cut angle. You might need to adjust those to have perfect fit. 
* gouge-led_holder.svg
  * 3mm plywood
  * the leds strips will lay on that plate so they won't fall in the spaces :) Also, make it easier to solder strips together.
* gouge-pcb_holder.svg
  * 3mm plywood
  * this is where the pcb/controller goes.
* gouge-spacer.svg
  * 3mm plywood
  * 3 of these are needed. They define the space required to properly light every words.


### cut notes:

The directory [svg/a4cut](svg/a4cut/) contains the file I actually used to instruct my laser cutter with K40Whisperer. Red lines are vector cut, Blue lines are vector engrave and Black means raster engrave.

Inserts are made to match specific kerf and cut angle of my cutter for these materiel. They might no work in another setup. You would be well advised to make experiments before cutting them :)

## electronics

Gouge use 65 ws2812b leds from a 60leds/m strip cut in smaller strips:
 * 7 strips of 8 leds
 * 1 strip of 5 leds
 * 1 strip of 4 leds

Pcb and other components can be found in (../pcb/clock_ws2812b/).

## code

Specific code for that clock can be found in [arduino/](arduino/). It heavily relies on the common libraries that can be found in (../libraries/).

## Why Gouge?

 Oh, you mean, project names have to mean something?

# Licence

All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](../LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).
