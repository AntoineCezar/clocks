#ifndef __have__RoundClockDisplay73_h__
#define __have__RoundClockDisplay73_h__

#include <ledstrip/LedStripFrame.h>
#include <ClockFace.h>

#define NUM_LEDS 60

class RoundClockFace: public ClockFace<LedStripFrame<NUM_LEDS>> {
    public:
    
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);

    private:
        int offset = 30;
};

#endif