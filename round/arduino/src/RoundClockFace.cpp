#include "RoundClockFace.h"

CRGB hour_color     = CRGB( 128,   0, 128 );
CRGB minute_color   = CRGB(   0,   0,  32 );
CRGB second_color   = CRGB( 100, 100, 100 );
CRGB hour_dot_color = CRGB( 128, 128,   0 );
CRGB hour_highlight = CRGB(   0, 196,  64 );

LedStripFrame<NUM_LEDS> RoundClockFace::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;
    for(int i=0; i< frame.nbLeds; i++) {
        frame.setPixelAt(i, CRGB::Black);
    }

    for(int i=0; i <= 12; i++) {
        frame.setPixelAt((this->offset + i*5)%60, hour_dot_color);
    }

    for(int dot = hour*5; dot <= hour*5 + minute; dot++) {
        if (dot % 5 == 0) 
            frame.setPixelAt((dot + this->offset)%60, hour_highlight);
        else 
            frame.setPixelAt((dot + this->offset)%60, minute_color);
    }
    
    if(second != 0)
        frame.setPixelAt((hour*5 + minute + second + this->offset)%60, second_color);

    frame.setPixelAt((hour*5 + this->offset)%60, hour_color);
    frame.setDuration(100);

    return frame;
}