#include <ZoneOffset.h>
#include <WifiLedClock.h>

#include <ledstrip/WS2812bDisplay.h>

#include "RoundClockFace.h"

HTTPClient httpClient;
String host = "zonetransition.herokuapp.com";

Zone zone(httpClient, host);

#define NUM_LEDS 60
#define DATA_PIN 2

int offset = 30;


ClockFace<LedStripFrame<NUM_LEDS>>* face = new RoundClockFace();
WS2812bDisplay<DATA_PIN, NUM_LEDS> *display = new WS2812bDisplay<DATA_PIN, NUM_LEDS>();

WifiLedClock<LedStripFrame<NUM_LEDS>>* ledClock = new WifiLedClock<LedStripFrame<NUM_LEDS>>("round", face, display, &zone);

void setup() {
    Serial.begin(115200);
    ledClock->setup();
}

void loop() {
    ledClock->loop();
}
