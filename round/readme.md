# round clock:

![a round clock](images/2021.04.20-round.144_inside.jpg)

This is a led interpretation of a classic wall clock, with leds replacing markings and the hands. It is made of 60 rgb leds (ws2812b).

There are two versions of that clock:
* inside: the led strip is oriented to face the center (inside) of the circle. This is the one shown in the picture
* outside: the led strip is facing the outside. Also as each hole are not close, it lights all its surrounding (quite a lot :). This version is in need of a lot of rework (at least a place to put the electronics :)