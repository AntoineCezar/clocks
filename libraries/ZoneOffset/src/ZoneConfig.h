#ifndef __CLOCK_CONFIG__
#define __CLOCK_CONFIG__

#include <Arduino.h>

class ZoneConfig {
    public:
        String getZoneOr(String defaultZone);
        void setZone(String zone);

        String getZoneTransitionHostOr(String defaultHost);
        void setZoneTransitionHost(String host);

    private:
        String zone;
        String host;

        void writeToFile(String filename, String value);
        String readFromFileOr(String filename, String defaultValue);
};

#endif
