#include "ZoneConfig.h"
#include <LittleFS.h>

String ZoneConfig::getZoneOr(String defaultZone) {
    return this->readFromFileOr("zone.txt", defaultZone);
}

void ZoneConfig::setZone(String zoneName) {
    this->writeToFile("zone.txt", zoneName);
}

String ZoneConfig::getZoneTransitionHostOr(String defaultHost) {
    return this->readFromFileOr("host.txt", defaultHost);
}

void ZoneConfig::setZoneTransitionHost(String host) {
    this->writeToFile("host.txt", host);
}

void ZoneConfig::writeToFile(String filename, String value) {
    LittleFS.begin();
    File f = LittleFS.open(filename, "w");
    f.print(value);
    f.close();
    LittleFS.end();
}

String ZoneConfig::readFromFileOr(String filename, String defaultValue) {
    String value = defaultValue;
    LittleFS.begin();
    if(LittleFS.exists(filename)) {
        File f = LittleFS.open(filename, "r");
        Serial.println("Reading from file");   
        value = f.readString();
        f.close();
    }
    LittleFS.end();

    return value;
}