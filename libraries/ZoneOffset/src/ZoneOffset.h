#ifndef __ZONE_H__
#define __ZONE_H__

#include <ESP8266HTTPClient.h>
#include <ZoneConfig.h>

typedef struct Transition {
    unsigned long epoch;
    int offset;
} Transition;

class Zone {
    public: 
        Zone(HTTPClient& http, String url);
        void updateTransition();
        void setZone(String zone);
        String currentZone();
        int getOffsetAt(unsigned long epoch);

        void changeZoneTransitionHost(String newUrl);
        String getZoneTransitionHost();

    private:
        const String DEFAULT_ZONE = "Europe/Paris";
        String host;
        int port = 80;
        HTTPClient* httpClient;
        ZoneConfig config;

        WiFiClient wifiClient;

        Transition transitions[5];
        String loadedZone;
        String wantedZone;
        int currentTransitionIdx = 0;

        String filenameOfZone(String zone);

        unsigned long nextUpdateTime = 0;
        unsigned long updateInterval = 15 * 24 * 60 * 60 * 1000;


        void readTransition(String zone);
        int loadTransition(String zone);
        boolean needToBeUpdated();
};

#endif