#ifndef __CLOCK_H__
#define __CLOCK_H__

#include <WiFiUdp.h>
#include <NTPClient.h> 

#include "ZoneOffset.h"

class Clock {

    public:
    Clock(Zone* zone, String defaultNTPPool) {
        this->zone = zone;
        this->ntpPool = readNTPPoolOr(defaultNTPPool);
        this->ntpClient = new NTPClient(ntpUDP, this->ntpPool.c_str(), 0, 3600 * 1000);
    }

    void begin();
    void update();
    int getHours();
    int getMinutes();
    int getSeconds();

    void changeNTPPool(String newPool);
    String getNTPPool();

    private:
    Zone* zone;
    String ntpPool;
    WiFiUDP ntpUDP;
    NTPClient* ntpClient;
    
    String readNTPPoolOr(String defaultNTPPool);
    void writeNTPPool(String ntpPool);

};

#endif
