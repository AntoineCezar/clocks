#ifndef __WIFI_LED_CLOCK_H__
#define __WIFI_LED_CLOCK_H__

#define WEBSERVER_H
#include <WiFiManager.h>

#include <LedClockDisplay.h>
#include <ZoneOffset.h>
#include <ResetWifiCredential.h>
#include "Clock.h"
#include "NetworkClockId.h"
#include "ConfigServer.h"
#include "ClockFace.h"
#include "DisplayQueue.h"

#include "OTA.h"
#include "OTAFeedbackDisplay.h"

template <class FRAME>
class WifiLedClock {

    public:
        WifiLedClock(String defaultName, ClockFace<FRAME>* face, LedClockDisplay<FRAME>* display, Zone* zone)
            : WifiLedClock(defaultName, face, display, zone, "europe.pool.ntp.org") {}

        WifiLedClock(String defaultName, ClockFace<FRAME>* face, LedClockDisplay<FRAME>* display, Zone* zone, String defaultNTPPool) {
            this->display = display;
            this->face = face;
            this->zone = zone;
            this->wifiReset = new ResetWifiCredential(0, &wifiManager);
            this->clock = new Clock(zone, defaultNTPPool);
            this->networkClockId = new NetworkClockId(defaultName);
            this->configServer = new ConfigServer<FRAME>(this->clock, this->networkClockId, this->zone, display->getDisplayQueue());
            this->ota = new OTA(display);
        };

        void setup() {
            display->displayPowerOn();

            wifiManager.autoConnect(this->networkClockId->getClockName().c_str());
            display->displayConnectedToWifi();
            
            clock->begin();
            display->displayNTPUpdated();

            clock->update();
            display->displayTransitionDownloaded(); 

            display->reset();

            this->networkClockId->begin();
            
            this->configServer->on("/firmware", HTTP_POST, ota->onRequest(), ota->onUpload());
            this->configServer->begin();  

            wifiReset->setup();
        }

        void loop() {
            ota->loop();
            if(ota->isUpdateInProgress()) {
                delay(500);
                return;
            }

            wifiReset->loop();
            this->networkClockId->update();
            this->clock->update();
            
            FRAME timeFrame = face->frameFor(this->clock->getHours(), this->clock->getMinutes(), this->clock->getSeconds());
            display->updateDisplay(&timeFrame);   
        }

    private:
    LedClockDisplay<FRAME>* display;
    ClockFace<FRAME>* face;
    Zone* zone;
    ResetWifiCredential *wifiReset;
    WiFiManager wifiManager;

    Clock* clock;    
    NetworkClockId* networkClockId;
    ConfigServer<FRAME>* configServer;
    OTA *ota;
    bool needToReboot = false;
};

#endif