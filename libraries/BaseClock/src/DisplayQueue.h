#ifndef __DISPLAY_QUEUE_H__
#define __DISPLAY_QUEUE_H__

#include <AsyncJson.h>
#include <FastLED.h>

template <class FRAME>
class DisplayQueue {
    
    public:
        DisplayQueue() {
            this->frames = new FRAME*[this->MAX_FRAMES];
            for(int i=0; i < this->MAX_FRAMES; i++) {
                this->frames[i] = nullptr;
            }
            this->currentlyDisplaying = new FRAME();
            this->toDisplay = new FRAME();
        }

        void queue(FRAME *frame) {
            this->lastAdded = (this->lastAdded + 1) % this->MAX_FRAMES;
            if(frames[this->lastAdded] != nullptr) {
                delete frames[this->lastAdded];
            }
            frames[this->lastAdded] = frame;
        }

        void queue(JsonVariant json) {
            FRAME* frame = new FRAME(json);
            this->queue(frame);
        }

        FRAME* nextFrame() {
            this->lastDisplayed = (this->lastDisplayed + 1) % this->MAX_FRAMES;
            return frames[this->lastDisplayed];
        };
        
        FRAME* frameToDisplay(FRAME* timeFrame) {
            if(this->currentlyDisplaying->getDuration() <=0 && !this->hasNext()) return timeFrame;
            
            if(this->currentlyDisplaying->getDuration() <= 0) {
                initFrameWith(currentlyDisplaying, this->nextFrame());
            }

            initFrameWith(toDisplay, currentlyDisplaying);
            toDisplay->add(timeFrame);
            toDisplay->setDuration(min(timeFrame->getDuration(), currentlyDisplaying->getDuration()));
            currentlyDisplaying->setDuration(currentlyDisplaying->getDuration() - toDisplay->getDuration());

            return toDisplay;
        };

    private:
        int lastAdded = -1;
        int lastDisplayed = -1;
        FRAME* currentlyDisplaying;
        FRAME* toDisplay;
        FRAME **frames;

        bool hasNext() {
            return this->lastAdded != this->lastDisplayed;
        }

        void initFrameWith(FRAME* destination, FRAME* source) {
            destination->clear();
            destination->add(source);
            destination->setDuration(source->getDuration());
        }


        const int MAX_FRAMES = 32;
};

#endif







