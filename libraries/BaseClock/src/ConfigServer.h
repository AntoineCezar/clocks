#ifndef __CONFIG_SERVER_H__
#define __CONFIG_SERVER_H__

#include <ESP8266WebServer.h>
#define WEBSERVER_H
#include <ESPAsyncWebServer.h>

#include "Clock.h"
#include "ZoneOffset.h"
#include "NetworkClockId.h"
#include "DisplayQueue.h"

#include <AsyncJson.h>


template <class FRAME>
class ConfigServer {

    public:
    ConfigServer(Clock* clock, NetworkClockId* networkClockId, Zone* zone, DisplayQueue<FRAME>* queue) {
        this->clock = clock;
        this->networkClockId = networkClockId;
        this->zone = zone;
        this->queue = queue;
    }

    void begin();
    void on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest, ArUploadHandlerFunction onUpload);
    
    private:
    Clock* clock;
    NetworkClockId* networkClockId;
    Zone* zone;
    DisplayQueue<FRAME>* queue;

    AsyncWebServer server = AsyncWebServer(80);

    void configureCORS(String origin);
};

template<class FRAME>
void ConfigServer<FRAME>::begin() {
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", "Hello, world");
    });

    server.on("/zone", HTTP_GET, [&] (AsyncWebServerRequest *request) {
        String message;
        if (request->params() == 0 ) {
            message = "Zone: " + this->zone->currentZone();
            message += "\n";
            message += "Host: " + this->zone->getZoneTransitionHost();
        }

        if (request->hasParam("name")) {
            message = request->getParam("name")->value();
            this->zone->setZone(request->getParam("name")->value());
        } 

        if (request->hasParam("host")) {
            message = request->getParam("host")->value();
            this->zone->changeZoneTransitionHost(request->getParam("host")->value());
        }


        request->send(200, "text/plain", message);
    });

    server.on("/network", HTTP_GET, [&] (AsyncWebServerRequest *request) {
        String message;
        if (request->hasParam("name")) {
            message = request->getParam("name")->value();
            this->networkClockId->changeName(request->getParam("name")->value());
        } else {
            message = this->networkClockId->getClockName();
        }
        request->send(200, "text/plain", message);
    });


    server.on("/version", HTTP_GET, [&] (AsyncWebServerRequest *request) {
        String message;
        message = "{ \"version\": \"";
        message += VERSION;
        message += "\", \"type\": \"";
        message += CLOCKTYPE;
        message += "\"}";
        request->send(200, "text/plain", message);
    });


    server.on("/clock", HTTP_GET, [&] (AsyncWebServerRequest *request) {
        String message;
        if (request->hasParam("pool")) {
            message = request->getParam("pool")->value();
            this->clock->changeNTPPool(request->getParam("pool")->value());
        } else {
            message = this->clock->getNTPPool();
        }
        request->send(200, "text/plain", message);
    });

    AsyncCallbackJsonWebHandler* handler = new AsyncCallbackJsonWebHandler("/display", [&](AsyncWebServerRequest *request, JsonVariant &json) {
        this->queue->queue(json);
        request->send(200, "text/plain", "OK");
    });

    server.addHandler(handler);
    configureCORS("*");

    server.begin();
}

template<class FRAME>
void ConfigServer<FRAME>::configureCORS(String origin) {
    DefaultHeaders::Instance().addHeader("Access-Control-Allow-Origin", origin);
    server.onNotFound([](AsyncWebServerRequest *request) {
        if (request->method() == HTTP_OPTIONS) {
            request->send(200);
        } else {
            request->send(404);
        }
    });
}

template<class FRAME>
void ConfigServer<FRAME>::on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest, ArUploadHandlerFunction onUpload) {
    server.on(uri, method, onRequest, onUpload);
}

#endif