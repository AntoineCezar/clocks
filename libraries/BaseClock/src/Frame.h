#ifndef __FRAME__H__
#define __FRAME__H__

class Frame {

    public: 
        void setDuration(int duration) {
            this->duration = duration;
        }

        int getDuration() {
            return duration;
        }

    private:
        int duration = 0;
};

#endif