#include "NetworkClockId.h"

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <LittleFS.h>

void NetworkClockId::begin() {
    WiFi.hostname(this->name);
    
    if (MDNS.begin(this->name)) {
        MDNS.addService("http", "tcp", 80);
    } else {
        Serial.println("Error setting up MDNS responder!");
    }
}

void NetworkClockId::update() {
    MDNS.update();
}

void NetworkClockId::changeName(String newName) {
    MDNS.removeService("http");
    MDNS.removeService("tcp");

    this->name = newName;
    this->writeClockName(newName);

    WiFi.hostname(this->name);

    MDNS.setHostname(this->name);
    MDNS.addService("http", "tcp", 80);
}

String NetworkClockId::getClockName() {
    return this->name;
}

void NetworkClockId::writeClockName(String name) {
    LittleFS.begin();
    File f = LittleFS.open("name.txt", "w");
    f.print(name);
    f.close();
    LittleFS.end();
    Serial.println("ClockConfig: name is set to: " + name );
}

String NetworkClockId::readNameOr(String defaultName) {
    String name = defaultName;
    LittleFS.begin();
    if(LittleFS.exists("name.txt")) {
        File f = LittleFS.open("name.txt", "r");
        name = f.readString();
        f.close();
    }
    LittleFS.end();
    return name;
}
