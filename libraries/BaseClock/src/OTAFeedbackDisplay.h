#ifndef __OTA_FEEDBACK_DISPLAY__
#define __OTA_FEEDBACK_DISPLAY__

class OTAFeedbackDisplay {
    public:
        virtual void updateInProgress(int progress) = 0;
        virtual void updateEnded() = 0;
        virtual void updateFailed() = 0;
};

#endif