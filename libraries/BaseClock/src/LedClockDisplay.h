#ifndef __LedClockDisplay_H__
#define __LedClockDisplay_H__

#include <Frame.h>

#include "DisplayQueue.h"
#include "OTAFeedbackDisplay.h"

template <class FRAME>
class LedClockDisplay : public OTAFeedbackDisplay {
    public:
        
        LedClockDisplay() {
            this->queue = new DisplayQueue<FRAME>();
        }

        void updateDisplay(FRAME* timeFrame) {
            FRAME* frame = this->queue->frameToDisplay(timeFrame);
            this->display(frame);   
        };

        DisplayQueue<FRAME>* getDisplayQueue() {
            return this->queue;
        }
        
        virtual void display(FRAME* frame) = 0;
        virtual void displayPowerOn() = 0;
        virtual void displayConnectedToWifi() = 0;
        virtual void displayNTPUpdated() = 0;
        virtual void displayTransitionDownloaded() = 0;
        virtual void reset() = 0;

    protected:
        DisplayQueue<FRAME>* queue;

};

#endif