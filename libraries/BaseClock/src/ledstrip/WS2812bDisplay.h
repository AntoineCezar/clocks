#ifndef __WS2812b_DISPLAY_H__
#define __WS2812b_DISPLAY_H__

#include "LedStripFrame.h"
#include "OTAFeedbackDisplay.h"
#include <FastLED.h>



template<int PIN, int NB_LEDS>
class WS2812bDisplay : public LedClockDisplay<LedStripFrame<NB_LEDS>> {
    public:
        WS2812bDisplay<PIN, NB_LEDS>(): LedClockDisplay<LedStripFrame<NB_LEDS>>() {
            FastLED.addLeds<WS2812B, PIN, GRB>(leds, NB_LEDS);
        };
        
        void display(LedStripFrame<NB_LEDS>* frame) {
            for (int i=0; i < nbLeds; i++) {
                this->leds[i] = frame->getPixelAt(i);
            }

            FastLED.show();
            delay(frame->getDuration());
        };

        void reset() {
            fill(CRGB::Black);
            FastLED.show();
        }

        void displayPowerOn() {
            fill(CHSV(192, 255, 64));
            FastLED.show();
        }

        void displayConnectedToWifi() {
            fill(CHSV(128, 255, 64));
            FastLED.show();
        }

        void displayNTPUpdated() {
            fill(CHSV(64, 255, 64));
            FastLED.show();
        }

        void displayTransitionDownloaded() {
            fill(CHSV(0, 255, 64));
            FastLED.show();
        }

        void fill(CRGB color) {
            for (int led =0; led < this->nbLeds; led++) {
                this->leds[led] = color;
            }
            FastLED.show();
        }

        void updateInProgress(int progress) {
            for(int led =0; led < nbLeds; led++) {
                if((led)*100/nbLeds <= progress)
                    this->leds[led] = CRGB(40, 40, 0);
                else
                    this->leds[led] = CRGB::Black;
            }
            FastLED.show();
        };

        void updateEnded() {
            fill(CRGB(0, 30, 0));
            FastLED.show();
        };

        void updateFailed() {
            fill(CRGB(30, 0, 0));
            FastLED.show();
        };

    private:
        const int pin = PIN;
        const int nbLeds = NB_LEDS;
        CRGB leds[NB_LEDS];
};

#endif