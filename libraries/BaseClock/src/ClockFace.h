#ifndef __CLOCKFACE_H__
#define __CLOCKFACE_H__

template <class FRAME>
class ClockFace {
    public:
        virtual FRAME frameFor(const int hour, const int minute, const int second) = 0;
};

#endif