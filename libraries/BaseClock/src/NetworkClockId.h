#ifndef __NETWORK_CLOCK_ID_H__
#define __NETWORK_CLOCK_ID_H__

#include <Arduino.h>

class NetworkClockId {

    public:
    NetworkClockId(String defaultName) {
        this->name = this->readNameOr(defaultName);
    }

    void begin();
    void update();
    void changeName(String newName);
    String getClockName();

    private:
    String name;

    void writeClockName(String name);
    String readNameOr(String defaultName);

};

#endif