#include "Clock.h"

#include <LittleFS.h>

void Clock::begin() {
    ntpClient->begin();

    int retry = 0;
    while(ntpClient->update() == false && retry < 10) {
        retry++;
        delay(1000);
    }
}

void Clock::update() {
    ntpClient->update();
    zone->updateTransition();

    ntpClient->setTimeOffset(0);
    int offset = zone->getOffsetAt(ntpClient->getEpochTime());
    
    ntpClient->setTimeOffset(offset);
}

int Clock::getHours() {
    return ntpClient->getHours();
}

int Clock::getMinutes() {
    return ntpClient->getMinutes();
}

int Clock::getSeconds() {
    return ntpClient->getSeconds();
}

void Clock::changeNTPPool(String newPool) {
    this->writeNTPPool(newPool);
    this->ntpPool = newPool;
    this->ntpClient->setPoolServerName(this->ntpPool.c_str());
}

void Clock::writeNTPPool(String ntpPool) {
    LittleFS.begin();
    File f = LittleFS.open("ntppool.txt", "w");
    f.print(ntpPool);
    f.close();
    LittleFS.end();

    this->ntpPool = ntpPool;
    Serial.println("Clock: ntpPool is set to: " + this->ntpPool );
}

String Clock::getNTPPool() {
    return this->ntpPool;
}

String Clock::readNTPPoolOr(String defaultNTPPool) {
    String ntpPool = defaultNTPPool;
    LittleFS.begin();
    if(LittleFS.exists("ntppool.txt")) {
        File f = LittleFS.open("ntppool.txt", "r");
        ntpPool = f.readString();
        f.close();
    }
    LittleFS.end();
    return ntpPool;
}