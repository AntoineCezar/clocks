#ifndef __have__LinearClockDisplay73_h__
#define __have__LinearClockDisplay73_h__

#include <ClockFace.h>
#include <ledstrip/LedStripFrame.h>

#define NUM_LEDS 73

class Linear73Face: public ClockFace<LedStripFrame<NUM_LEDS>> {
    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);
};

#endif