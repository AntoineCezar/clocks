#include "Linear73Face.h"

CRGB hour_color     = CRGB( 128,   0, 128 );
CRGB hour_color_low     = CRGB( 8,   0, 8 );
CRGB minute_color   = CRGB(   0,   0,  16 );
CRGB second_color   = CRGB( 100, 100, 100 );
CRGB hour_dot_color       = CRGB( 128, 128,   0 );
CRGB hour_highlight = CRGB(   0, 196,  64 );
CRGB hour_highlight_low = CRGB(   0, 49,  16 );

LedStripFrame<NUM_LEDS> Linear73Face::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;

    for(int i=0; i< frame.nbLeds; i++) {
        frame.setPixelAt(i, CRGB::Black);
    }

    for(int i=0; i <= hour%12; i++) {
        CRGB color;
        if (i == 6) 
            color = hour_highlight;
        else
            color = hour_highlight_low;

        int hour_dot = i*6;
        frame.setPixelAt(hour_dot, color);
    }

    for(int i=hour%12; i <=12; i++) {
        CRGB color;
        if (i == 6) 
            color = hour_color;
        else
            color = hour_color_low;

        int hour_dot = i*6;
        frame.setPixelAt(hour_dot, color);
    }

    if(hour == 12) {
        frame.setPixelAt(hour*6, hour_dot_color);
    } else {
        frame.setPixelAt((hour%12)*6, hour_dot_color);
    }

    for(int dot = 1; dot <= minute; dot++) {
        int minute_dot = dot + (dot - 1)/5;
        frame.setPixelAt(minute_dot, minute_color);
    }

    if(second != 0) {
        int second_dot = second + (second - 1)/5;
        frame.setPixelAt(second_dot, second_color);
    }

    frame.setDuration(100);

    return frame;
}