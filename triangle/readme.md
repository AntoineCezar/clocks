# Triangle: like square but with triangle. And a twist. And gradients.

![a clock made of trianble](triangle/images/2020.08.19-triangle.1024px.jpg)

Triangle is a clock that uses triangle to tell time.

It uses 36 ws2812b leds. But currently only 27 are really used. I have not figured out what to do with the remaining 9 (the central triangle).

The pcb for electronic can be found in the [`pcb/clockk_ws2812b` directory](https://gitlab.com/avernois/clocks/-/tree/main/pcb/clock_ws2812b)


