#include "TriangleClockFace.h"

#define MAX_HUE_VALUE 255
int color_shift = 0;


int HOUR_UNIT_STRIP[9]   =   {  0,  1,  2, 19,  3, 18,  4, 17, 20 };
int MINUTE_UNIT_STRIP[9] =   { 10,  9,  8, 11,  7, 12,  6, 13, 26 };

int UNIT[10][9]          = { {  0,  0,  0,  0,  0,  0,  0,  0,  0 },
                             {  1,  0,  0,  0,  0,  0,  0,  0,  0 },
                             {  0,  0,  1,  1,  0,  0,  0,  0,  0 },
                             {  0,  1,  0,  0,  1,  1,  0,  0,  0 },
                             {  0,  0,  1,  1,  1,  1,  0,  0,  0 },
                             {  1,  1,  0,  0,  0,  0,  1,  1,  1 },
                             {  1,  1,  0,  0,  1,  1,  1,  0,  1 },
                             {  1,  1,  1,  1,  1,  1,  0,  1,  0 },
                             {  1,  1,  1,  1,  1,  1,  1,  0,  1 },
                             {  1,  1,  1,  1,  1,  1,  1,  1,  1 }
                           };

int HOUR_TENS_STRIP[4]   =   { 31, 30, 29, 32 };
int HOUR_TENS[3][4]      = { {  0,  0,  0,  0 },
                             {  1,  0,  0,  0 },
                             {  0,  0,  1,  1 }
                           };

int MINUTE_TENS_STRIP[5] =   { 27, 28, 34, 33, 35 };
int MINUTE_TENS[6][5]    = { {  0,  0,  0,  0,  0 },
                             {  0,  0,  1,  0,  0 },
                             {  0,  1,  0,  1,  0 },
                             {  1,  0,  1,  0,  1 },
                             {  1,  1,  0,  1,  1 },
                             {  1,  1,  1,  1,  1 } 
                           };

LedStripFrame<NUM_LEDS> TriangleClockFace::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;
    int gradient_step = 6;
    
    int minute_tens = minute / 10;
    int minute_unit = minute % 10;
    int hour_tens = hour / 10;
    int hour_unit = hour % 10;
    
    for(int i=0; i < 9; i++) {
        if(UNIT[minute_unit][i] == 0)
            frame.setPixelAt(MINUTE_UNIT_STRIP[i], CHSV((color_shift + gradient_step*i + MAX_HUE_VALUE/3) % MAX_HUE_VALUE, 255, 32));
        else
            frame.setPixelAt(MINUTE_UNIT_STRIP[i], CHSV((color_shift + gradient_step*i + MAX_HUE_VALUE/3) % MAX_HUE_VALUE, 255, 255));
    }

    for(int i=0; i < 5; i++) {
        if(MINUTE_TENS[minute_tens][i] == 0)
            frame.setPixelAt(MINUTE_TENS_STRIP[i], CHSV((color_shift + gradient_step*(i + 8) + MAX_HUE_VALUE/3) % MAX_HUE_VALUE, 255, 32));
        else
            frame.setPixelAt(MINUTE_TENS_STRIP[i], CHSV((color_shift + gradient_step*(i + 8) + MAX_HUE_VALUE/3) % MAX_HUE_VALUE, 255, 255));
    }

    for(int i=0; i < 9; i++) {
        if(UNIT[hour_unit][i] == 0)
            frame.setPixelAt(HOUR_UNIT_STRIP[i], CHSV((color_shift + gradient_step*i) % MAX_HUE_VALUE, 255, 32));
        else
            frame.setPixelAt(HOUR_UNIT_STRIP[i], CHSV((color_shift + gradient_step*i) % MAX_HUE_VALUE, 255, 255));

    }

    for(int i=0; i < 4; i++) {
        if(HOUR_TENS[hour_tens][i] == 0)
            frame.setPixelAt(HOUR_TENS_STRIP[i], CHSV((color_shift + gradient_step*(i + 8)) % MAX_HUE_VALUE, 255, 32));
        else
            frame.setPixelAt(HOUR_TENS_STRIP[i], CHSV((color_shift + gradient_step*(i + 8)) % MAX_HUE_VALUE, 255, 255));

    }

    color_shift = (color_shift + 1) % MAX_HUE_VALUE;

    frame.setDuration(125);

    return frame;
}