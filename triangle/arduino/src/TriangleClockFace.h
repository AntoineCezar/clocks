#ifndef __have__TriangleClockDisplay_h__
#define __have__TriangleClockDisplay_h__

#include <ledstrip/LedStripFrame.h>
#include <ClockFace.h>

#define NUM_LEDS 36

class TriangleClockFace: public ClockFace<LedStripFrame<NUM_LEDS>> {
    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);
};

#endif