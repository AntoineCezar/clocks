#ifndef __have__HexagonFace_h__
#define __have__HexagonFace_h__

#include <ledstrip/LedStripFrame.h>
#include <ClockFace.h>

#define NUM_LEDS 88

class HexagonFace: public ClockFace<LedStripFrame<NUM_LEDS>> {
    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);

    private:
        const int intensity = 92;
        int hue_offset = 0;
        int previous_second = 0;
        int previous_minute = 0;
        int previous_hour = 0;
        int fade = 0;
        int DIGIT[6][13] =  {
            { 0, 1, 2 ,3, 4, 5, 6, 7, 8, 10, 11, 12, 13},
            { 18, 17, 16, 15, 23, 22, 21, 20, 19, 28, 27, 26, 25 },
            { 30, 31, 32, 33, 34, 35, 36, 37, 38, 40, 41, 42, 43 },
            { 48, 47, 46, 45, 53, 52, 51, 50, 49, 58, 57, 56, 55 },
            { 60, 61, 62, 63, 64, 65, 66, 67, 68, 70, 71, 72, 73 },
            { 78, 77, 76, 75, 83, 82, 81, 80, 79, 87, 86, 85, 84 }
        };

        void fillNumber(LedStripFrame<NUM_LEDS> *frame, int previous, int current, int fade, int max_intensity, int digit_place);
};

#endif