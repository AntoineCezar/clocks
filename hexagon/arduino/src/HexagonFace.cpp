#include "HexagonFace.h"

CRGB hour_color     = CRGB( 128,   0, 128 );
CRGB minute_color   = CRGB(   0,   0,  32 );
CRGB second_color   = CRGB( 100, 100, 100 );
CRGB hour_dot_color = CRGB( 128, 128,   0 );
CRGB hour_highlight = CRGB(   0, 196,  64 );

int NUMBER[10][13] = {
    {1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1},
    {1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1},
    {1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1},
    {0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1},
    {1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1},
    {0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1},
    {1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1}
};

LedStripFrame<NUM_LEDS> HexagonFace::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;

    for(int i=0; i< frame.nbLeds; i++) {
        frame.setPixelAt(i, CRGB::Black);
    }

    int second_unit = second % 10;
    int second_tenth = second / 10;
    int minute_unit = minute % 10;
    int minute_tenth = minute / 10;
    int hour_unit = hour % 10;
    int hour_tenth = hour / 10;

    int previous_hour_tenth = this->previous_hour / 10;
    int previous_hour_unit = this->previous_hour % 10;
    int previous_minute_tenth = this->previous_minute / 10;
    int previous_minute_unit = this->previous_minute % 10;
    int previous_second_tenth = this->previous_second / 10;
    int previous_second_unit = this->previous_second % 10;

    int hue = this->hue_offset;
    this->hue_offset = hue / 2;

    fillNumber(&frame, previous_hour_tenth,   hour_tenth,   this->fade, this->intensity, 0);
    fillNumber(&frame, previous_hour_unit,    hour_unit,    this->fade, this->intensity, 1);
    fillNumber(&frame, previous_minute_tenth, minute_tenth, this->fade, this->intensity, 2);
    fillNumber(&frame, previous_minute_unit,  minute_unit,  this->fade, this->intensity, 3);
    fillNumber(&frame, previous_second_tenth, second_tenth, this->fade, this->intensity, 4);
    fillNumber(&frame, previous_second_unit,  second_unit,  this->fade, this->intensity, 5);

    if(this->previous_second != second || this->previous_minute != minute || this->previous_hour != hour) {
        fade = fade + 16;
        if(fade > this->intensity) {
            fade = 0;
            this->previous_second = second;
            this->previous_minute = minute;
            this->previous_hour = hour;
        }
    }


    this->hue_offset = (hue + 1)%(255*2);

    frame.setDuration(50);

    return frame;
}

void HexagonFace::fillNumber(LedStripFrame<NUM_LEDS> *frame, int previous, int current, int fade, int max_intensity, int digit_place) {
    int hue = this->hue_offset + 255/6*digit_place;

    for(int i=0; i<13; i++) {
        if(NUMBER[current][i] == 1 && NUMBER[previous][i] == 0)
            frame->setPixelAt(DIGIT[digit_place][i], CHSV(hue, 255, fade));
        
        if(NUMBER[previous][i] == 1 && NUMBER[current][i] == 0)
            frame->setPixelAt(DIGIT[digit_place][i], CHSV(hue, 255, this->intensity - fade));

        if(NUMBER[previous][i] == 1 && NUMBER[current][i] == 1)
            frame->setPixelAt(DIGIT[digit_place][i], CHSV(hue, 255, this->intensity));
    }
}