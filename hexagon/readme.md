# Hexagon: a readable clock with hexagons

Hexagon uses an hexagon based display to tell time, hours, minutes and seconds.

![hexagon](images/2021.09.25-hexagon.1024px.jpg)


## Licence
All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](../LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).
