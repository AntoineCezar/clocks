# Clocks: a repository for clocks

I love to make clocks, usually with wood, leds and weird time representation :)

This repository is an attempt to collect all the clock I already made, and the future ones in the same place.

One of the goal is to centralize the code that is common to all of them in hte same place and make it easy to build a new one on top of it. That allows all the clocks to share some nice features like:
* wifi configuration.
* NTP synchronisation.
* time zone configuration
* automatic offset management (with the help of my [Zone Transition server](https://gitlab.com/avernois/zone-transition)).
* mDNS (zeroconf) configuration
* basic Over The Air update of the firmware

## [Acrylic Word Clock](acrylic_word_clock/): a word clock on an acrylic plate

![acrylic word clock](acrylic_word_clock/images/wordclock_syncopate-800x600.jpg)

## [Gouge](gouge/): a word clock in a box
![gouge](gouge/images/2020.11.11-gouge.1024px.jpg)

## [Hexagon]{hexagon/}: a readable clock with hexagons
![hexagon](hexagon/images/2021.09.25-hexagon.1024px.jpg)

## [Linear](linear/): a clock on a line

![a small linear clock](linear/images/2020.07.20-linear_144.1024px.jpg)

![a large linear clock](linear/images/2019.09.28-linear_clock.039.1024px.jpg)

## [Round](round/): a led approach of a traditional clock

![a round clock - led facing inside](round/images/2021.04.20-round.144_inside.jpg)

## [Square](square): a TIX inspired clock

![a square clock](square/images/2020.08.03-square-hor.1024px.jpg)

## [Triangle](triangle): like square but with triangle. And a twist. And gradients.

![a clock made of trianble](triangle/images/2020.08.19-triangle.1024px.jpg)

## [4 leds](4_leds/): a small clock with 4 leds

![a small clock with 4 leds](4_leds/images/2020.06.04-4_leds.1024px.jpg)

# PCB
Clocks based on a ws2812b led strip are all using the same pcb that you can found in [pcb/clock_esp8266](pcb/clock_ws2812b/).


# Issue and feature requests
If you find an issue or have an awesome idea for a new feature, or improve an exiting one, please open an issue. Please give as much detail as possible.

Contributions are welcomed, but if you plan on your code or design to be merge here I strongly suggest that we talk about it before you put too much work on it. (I have strong opinion on the direction that project will be going :)

# Licence

All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).

# Encouraging me to make more clocks

If you like my clocks and like to encourage my addiction to building new ones, you can:
* tell it to me: that's always good for my ego :) (publicly or privately, [my twitter](https://twitter.com/avernois) dm are open)
* buy some useful stuff from that [amazon list](https://amzn.eu/4P2MJ4O) (if you have an alternative to amazon for that kind of lists, let me know :)
