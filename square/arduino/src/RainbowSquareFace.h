#ifndef __RAINBOWSQUAREDISPLAY_H__
#define __RAINBOWSQUAREDISPLAY_H__

#include "SquareFace.h"

class RainbowSquareFace : public SquareFace {
    public:
    LedStripFrame<NUM_LEDS> frameFor(int hour, int minute, int second);
    
    private:
    void fill_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color);
    void fill_cadre_gradient(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color);
    void display_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], int number, CRGB baseColor);
};

#endif