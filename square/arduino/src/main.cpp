#include <ZoneOffset.h>
#include <WifiLedClock.h>

#include <ledstrip/WS2812bDisplay.h>

#include "SolidSquareFace.h"
#include "DiceFace.h"
#include "RainbowSquareFace.h"

#define NUM_LEDS 36
#define DATA_PIN 2

HTTPClient httpClient;
String host = "zonetransition.herokuapp.com";

Zone zone(httpClient, host);

ClockFace<LedStripFrame<NUM_LEDS>>* face = new RainbowSquareFace();
WS2812bDisplay<DATA_PIN, NUM_LEDS> *display = new WS2812bDisplay<DATA_PIN, NUM_LEDS>();

WifiLedClock<LedStripFrame<NUM_LEDS>>* ledClock = new WifiLedClock<LedStripFrame<NUM_LEDS>>("square", face, display, &zone);

void setup() {
    Serial.begin(115200);
    ledClock->setup();
}

void loop() {
    ledClock->loop();
}
