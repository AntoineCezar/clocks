#include "SolidSquareFace.h"

#include <FastLED.h>

void SolidSquareFace::fill_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color) {
    for(int i = 0; i < 9; i++) {
            frame->setPixelAt(CADRE[i], color);
        }
}

void SolidSquareFace::display_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], int number, CRGB baseColor) {
    if(number == 9) {
        fill_cadre(frame, CADRE, baseColor);
        return;
    }

    if(number == 0) {
        fill_cadre(frame, CADRE, CRGB::Black);
        return;
    }

    int previouslyLit[9];
    int nbLit = 0;
    for(int i=0; i <9; i++) {
        if(frame->getPixelAt(CADRE[i])) {
            previouslyLit[nbLit] = i;
            nbLit++;
        }
    }
    
    if(nbLit > 0) {
        int toLitOff = random(0, nbLit);
        frame->setPixelAt(CADRE[previouslyLit[toLitOff]], CRGB::Black);
        nbLit = nbLit - 1;
    }

    while(nbLit < number){
        int rand = random(0, 9);
        if (!frame->getPixelAt(CADRE[rand])) {
            frame->setPixelAt(CADRE[rand], baseColor);
            nbLit++;
        }
    }
}

void SolidSquareFace::serialPrint(int hour, int minute, int second) {
    Serial.print("display: ");
    Serial.print(hour);
    Serial.print(":");
    Serial.print(minute);
    Serial.print(":");
    Serial.println(second);
}

LedStripFrame<NUM_LEDS> SolidSquareFace::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;
    int toDisplay[4] =  { minute%10, minute/10, hour%10, hour/10 };

    for(int i=0; i < 4; i++) {
        display_cadre(&frame, CADRE[i], toDisplay[i], colors[i]);
    }

    frame.setDuration(1000);

    return frame;
}   