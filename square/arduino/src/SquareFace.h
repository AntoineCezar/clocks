#ifndef __SQUAREDISPLAY_H__
#define __SQUAREDISPLAY_H__

#include <ledstrip/LedStripFrame.h>
#include <ClockFace.h>

#define NUM_LEDS 36

class SquareFace: public ClockFace<LedStripFrame<NUM_LEDS>> {
    public:
        virtual LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second) = 0;

    protected:
        const int CADRE[4][9] = { { 9, 10, 11, 14, 13, 12, 33, 34, 35 }, 
                                { 6,  7,  8, 17, 16, 15, 30, 31, 32 },
                                { 3,  4,  5, 20, 19, 18, 27, 28, 29 },
                                { 0,  1,  2, 23, 22, 21, 24, 25, 26 } };
};

#endif