#ifndef __DICEDISPLAY_H__
#define __DICEDISPLAY_H__

#include "SquareFace.h"
#include <FastLED.h>

class DiceFace : public SquareFace {
    public:
    LedStripFrame<NUM_LEDS> frameFor(int hour, int minute, int second);
    
    private:
    void display_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], int number, CRGB baseColor);
    void fill_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color);
};

#endif