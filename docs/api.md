# HTTP API

## Introduction

This page documents http endpoint that can be used to interact and configure the clock.

!!! warning "HTTP only"
    Currently, HTTPS is not supported as I yet have to figure a way to embed reliable ssl certificates (and a way to update them) in the device.

!!! warning "no security"
    Call to the api are not secured by any means. Anyone with access on the same network as the clock can access the api. It would be a bad[^bad] idea to connect that clock on a network where untrusted people can join.

## Configuration

!!! note "GET /zone?name=[zone]&host=[host]"

    If no parameters are given, the call return the current values in plain text.

    Option    | Type   |          | Description
    --------- | ------ | ---------| -----------
    `zone`    | string | optional | name of the time zone to set. *Example: Europe/Paris*.
    `host`    | string | optional | url of the [Zone Transition](https://gitlab.com/avernois/zone-transition) server to use.


!!! note "GET /network?name=[hostname]"

    Get and set the clock hostname.

    If no parameters are given, the call returns the current hostname in plain text.

    Option    | Type   |          | Description
    --------- | ------ | ---------| -----------
    `hostname`| string | optional | hostname of the clock.


    The hostname is used by the clock to presents itself on the network. 
    Currently, it is used when registering to the dhcp and the mdns.

    If `triangle` is the hostname of a clock, then it should be accessible at `http://triangle.local/` from another device connected to the same network (if it manages mDNS (aka bonjour, zeroconf)).

    Because network is not an immediate thing, when the name is changed, it might take some times to be effective.

## Information

!!! note "GET /version"

    Get information about the firmware. It will return a message in JSON format: 
    ex: 

    ``{ "version": "0.1", "type": "triangle"}``

    where:

    * ``version``: is the version of the current firmware as set when it was built
    * ``type``: the type of the clock


## Display

!!! note "POST /display"

    ```
    {
        "strip":
            [ {"color": "#FF0000", "led": 0 },
            {"color": "#00FF00", "led": 1 },
                {"color": "#0000FF", "led": 3 }
            ],
        "duration": 2000
    }
    ```

    Where:

    * ``duration``: the frame will be displayed for that amount of time in milliseconds.
    * ``strip``: described the state of each led in the led strip.
        * ``color``: the color as RGB in hexa
        * ``led``: the number of the led

    Note: if a led in the strip is not assigned to a color, it will be assigned to #000000.

    This will put the given frame in a queue to be displayed on a first in first out base.
    Frame will be displayed for the given `duration` time, expressed in milliseconds.

    Note: the size of the queue ``MAX_FRAMES`` is set in ``BaseClock/src/DisplayQueue.h``.

    Note: currently, the given frame will not be displayed as given. Each color will be added to the color that the display would have displayed otherwise.

[^bad]: or a fun experiment? :)