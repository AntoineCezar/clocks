# Crafting Labs - Clocks documentation

This is the documentation for the [Clocks](https://gitlab.com/avernois/clocks) project by Crafting Labs.

If you don't have you own clock, you can buy it from [the shop](https://shop.crafting-labs.fr/en)

## Internet connection

Clocks use internet to retrieve time zone offset for you time zone and synchronize time via NTP.
To work properly, a clock need to connect to a wifi network connected to internet[^internet].

### Set wifi credentials

The first time you start the clock[^first], it will will start a wifi access point. 
By default, the name of the access point is the name of your clock (so `gouge` for a *Gouge*, `triangle` for a *Triangle*, ...).

Connect to that network from another device (computer, smartphone, ...). 
That network does not require a password.

Once connected, your device should automatically redirect you (or ask you to go) to a webpage that should propose you to configure your wifi. 
If your system does not propose you to go there, you can manually navigate to [http://192.168.4.1/](http://192.168.4.1/).

Once you are on the configure page, go to `Configure WiFi` then select your network from the list and set its password. 
When done, click save and you should be done. 
The clock will restart and connect to you wifi network and start synchronizing. 

It might need a few minutes to display the correct time, but once connected to internet, it'll get there eventually :) 

If after saving the wifi credentials, the clock still can't connect to the network (the display stays purple) it will restart the access point waiting for the credentials[^connect].

Note: if your network has access restriction based on MAC Adress, you will have to add the MAC of the clocks to the allowed list, you can get the information in the "Info" tabs on the configuration page (when connected to the access point).


### Reset wifi credentials

If you need to connect the clock to another network while the previous one is still accessible:
* open the clock
* locate the "flash button" on the purple pcb
* push that button for at least 10 seconds.

The clocks should then restart and start the configuration access point (see [above](#set-wifi-credentials)).

!!! hint
    If the previously configured network is not available, simply power off/on the clock and it will start the configuration access point as if it was its first start (see [above](#set-wifi-credentials)).


## Clock configuration

The system does not yet have a proper configuration page. It will come. Eventually.

For now, to configure the clocks, you will have to use the [API](api.md).


[^internet]: technically, it does not need to be internet. It only needs access to an NTP server and a ZoneTransition server. If you have those running on your network, you could configure your clock to used them instead of the default one.
[^first]: or every time it can't find the network it is supposed to connect to.
[^connect]: it will do so every time it starts and can't connect to a network