# PCB for clocks using esp8266 and ws2812b leds strip

![a soldered pcb](images/2020.07.20-ws2812b_esp.1024px.jpg)

![a soldered pcb with parts removed](images/2020.07.20-ws2812b_esp_parts.1024px.jpg)

The directory contains schematics, netlist and pcb layout for the pcb used in most of clocks using ws2812b leds strip.

They are made using kicad 5. 

If you want to see/edit the schematics, you might need to get my [custom kicad components/footprint library](https://gitlab.com/avernois/kicad-lib) (and probably fix some directory links, as I did not manage to make them properly relative/independent).

## Bill of material

* SW1: SMD tactile button 4.5x4.5x3mm
* SW2: SMD tactile button 4.5x4.5x3mm
* U1 : HT7333 (3,3V linear voltage regulator)
* U2 : 2 8 pin headers (to connect an ESP12 on a breakout board)
* J1 : 5 pins header (to connect a micro USB breakout)
* J2 : 5 pins header (to rx/tx/gnd + optional external flash/reset button)
* J3 : 3 pins connector (to leds strip)
* C1 : 47uF capacitor
* C2 : 33uF capacitor

## Order

You can directly order the pcb (without component) from [OSHPark](https://oshpark.com/shared_projects/edw04z1X) (based in the US).
That's were I order my small batch/prototype pcb.