EESchema Schematic File Version 4
LIBS:loader-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x05_Male J2
U 1 1 5E3EC212
P 5200 1900
F 0 "J2" V 5260 2141 50  0000 L CNN
F 1 "to Esp" V 5351 2141 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Horizontal" H 5200 1900 50  0001 C CNN
F 3 "~" H 5200 1900 50  0001 C CNN
	1    5200 1900
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x06_Female J1
U 1 1 5E3EC328
P 5150 2700
F 0 "J1" H 5044 2175 50  0000 C CNN
F 1 "ftdi" H 5044 2266 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Horizontal" H 5150 2700 50  0001 C CNN
F 3 "~" H 5150 2700 50  0001 C CNN
	1    5150 2700
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5E3EC931
P 4100 2050
F 0 "SW1" H 4100 2335 50  0000 C CNN
F 1 "Flash" H 4100 2244 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm_Wuerth-430476085716" H 4100 2250 50  0001 C CNN
F 3 "~" H 4100 2250 50  0001 C CNN
	1    4100 2050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5E3EC982
P 4100 1650
F 0 "SW2" H 4100 1935 50  0000 C CNN
F 1 "Reset" H 4100 1844 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm_Wuerth-430476085716" H 4100 1850 50  0001 C CNN
F 3 "~" H 4100 1850 50  0001 C CNN
	1    4100 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1650 3650 1650
Wire Wire Line
	3900 2050 3650 2050
Text GLabel 5400 2400 2    50   Input ~ 0
GND
Text GLabel 3650 1650 0    50   Input ~ 0
GND
Text GLabel 3650 2050 0    50   Input ~ 0
GND
Wire Wire Line
	5400 1700 5400 2550
Wire Wire Line
	5400 2550 5350 2550
Wire Wire Line
	5350 2550 5350 2900
Wire Wire Line
	5300 1700 5300 2550
Wire Wire Line
	5300 2550 5250 2550
Wire Wire Line
	5250 2550 5250 2900
Wire Wire Line
	5200 1700 5200 2550
Wire Wire Line
	5200 2550 5150 2550
Wire Wire Line
	5150 2550 5150 2900
Wire Wire Line
	5000 1700 4300 1700
Wire Wire Line
	4300 1700 4300 1650
Wire Wire Line
	5100 1700 5100 2050
Wire Wire Line
	5100 2050 4300 2050
NoConn ~ 5450 2900
NoConn ~ 5050 2900
NoConn ~ 4950 2900
$EndSCHEMATC
