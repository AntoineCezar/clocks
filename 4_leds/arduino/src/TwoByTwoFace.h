#ifndef __TWO_BY_TWO_FRAME_H__
#define __TWO_BY_TWO_FRAME_H__

#include "ledstrip/LedStripFrame.h"
#include "ClockFace.h"

#define NUM_LEDS 4

class TwoByTwoFace : public ClockFace<LedStripFrame<NUM_LEDS>> {

    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second) {
            LedStripFrame<NUM_LEDS> frame;
            String time = "";
            time = time + hour + ":" + minute + ":" + second;
            Serial.println(time);
            frame.setPixelAt(0, CHSV(hour * 255/12, 255, value));
            frame.setPixelAt(3, CHSV(millis()%1000 * 255/1000, 255, value));
            frame.setPixelAt(1, CHSV(minute * 255/60, 255, value));
            frame.setPixelAt(2, CHSV(second * 255/60, 255, value));
        
            frame.setDuration(500);

            return frame;
        };

    private:
        const int value = 64;
};

#endif