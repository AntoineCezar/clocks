# 4 leds: a small clock with 4 leds

![a small clock with 4 leds](4_leds/images/2020.06.04-4_leds.1024px.jpg)

It uses 4 leds to display time. The color of the two upper leds tells the hour, the lower left is the minute et lower right is the second.

Disclaimer: this one is not really made to be readable. I needed a small toy I can keep in my bag to work easily on the software components from (almost) anywhere.
But also, it's kind of cute, so maybe I'll find a way to make it readable :)