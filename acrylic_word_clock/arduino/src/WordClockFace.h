#ifndef __have__WordClockDisplay_h__
#define __have__WordClockDisplay_h__

#include <ledstrip/LedStripFrame.h>
#include <ClockFace.h>

#define NUM_LEDS 24

class WordClockFace  : public ClockFace<LedStripFrame<NUM_LEDS>> {

    public:
    LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);

    private:
    const int nbLeds = 24;
    enum word_led_index {
        H_UNE = 0, H_DEUX, H_TROIS, H_QUATRE, H_CINQ, H_SIX, H_SEPT, H_HUIT, H_NEUF, H_DIX, H_ONZE, MIDI, MINUIT, HEURE, HEURES, 
        ET, MOINS, LE, 
        M_DIX, M_QUART, M_VINGT, M_CINQ, M_DEMI, M_DEMIE,
        NONE
    };

    struct TEXT_LED_INDEXES {
        int nbWords;
        const word_led_index *w;
    };

    const word_led_index minuit[1] = {MINUIT};
    const word_led_index une_heure[2] = {H_UNE, HEURE};
    const word_led_index deux_heures[2] = {H_DEUX, HEURES};
    const word_led_index trois_heures[2] = {H_TROIS, HEURES};
    const word_led_index quatre_heures[2] = {H_QUATRE, HEURES};
    const word_led_index cinq_heures[2] = {H_CINQ, HEURES};
    const word_led_index six_heures[2] = {H_SIX, HEURES};
    const word_led_index sept_heures[2] = {H_SEPT, HEURES};
    const word_led_index huit_heures[2] = {H_HUIT, HEURES};
    const word_led_index neuf_heures[2] = {H_NEUF, HEURES};
    const word_led_index dix_heures[2] = {H_DIX, HEURES};
    const word_led_index onze_heures[2] = {H_ONZE, HEURES};
    const word_led_index midi[1] = {MIDI};

    struct TEXT_LED_INDEXES HOUR_LEDS[13] = {
        {1, minuit},
        {2, une_heure},
        {2, deux_heures},
        {2, trois_heures},
        {2, quatre_heures},
        {2, cinq_heures},
        {2, six_heures},
        {2, sept_heures},
        {2, huit_heures},
        {2, neuf_heures},
        {2, dix_heures},
        {2, onze_heures},
        {1, midi}
    };

    const word_led_index pile[] = {};
    const word_led_index cinq[1] = {M_CINQ};
    const word_led_index dix[1] = {M_DIX};
    const word_led_index et_quart[2] = {ET, M_QUART};
    const word_led_index vingt[1] = {M_VINGT};
    const word_led_index vingt_cinq[2] = {M_VINGT, M_CINQ};
    const word_led_index et_demi[2] = {ET, M_DEMI};
    const word_led_index et_demie[2] = {ET, M_DEMIE};
    const word_led_index moins_vingt_cinq[3] = {MOINS, M_VINGT, M_CINQ};
    const word_led_index moins_vingt[2] = {MOINS, M_VINGT};
    const word_led_index moins_le_quart[3] = {MOINS, LE, M_QUART};
    const word_led_index moins_dix[2] = {MOINS, M_DIX};
    const word_led_index moins_cinq[2] = {MOINS, M_CINQ};

    struct TEXT_LED_INDEXES MINUTE_LEDS[13] = {
        {0, pile},
        {1, cinq},
        {1, dix},
        {2, et_quart},
        {1, vingt},
        {2, vingt_cinq},
        {2, et_demie},
        {3, moins_vingt_cinq},
        {2, moins_vingt},
        {3, moins_le_quart},
        {2, moins_dix},
        {2, moins_cinq},
        {2, et_demi}
    };


    void deactivateAllLeds(LedStripFrame<NUM_LEDS>* frame);
    void activateLeds(LedStripFrame<NUM_LEDS>* frame, TEXT_LED_INDEXES word_leds);
    CRGB activeColorForLed(int ledNumber);
    CRGB inactiveColorForLed(int ledNumber);
};

#endif