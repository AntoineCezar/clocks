# Acrylic word clock

![a word clock](images/wordclock_syncopate-800x600.jpg)

This is a clock that tell time in french quite similar in spirit to [Gouge](https://gitlab.com/avernois/gouge). 
It's made from laser cut wood and acrylic, an esp8266, 24 ws2812b leds and some other components.

It is strongly inspired from (https://hackaday.io/project/27467-word-clock-base-on-rgb-led-ws2812b).

The pcb for electronic can be found in the [`pcb/clockk_ws2812b` directory](https://gitlab.com/avernois/clocks/-/tree/main/pcb/clock_ws2812b)
